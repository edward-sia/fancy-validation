import React, { Component } from 'react'
import PropTypes from 'prop-types'

const formed = (ComponentToWrap) => {
  return class FormComponent extends Component {
    // let’s define what’s needed from the `context`
    static contextTypes = {
      form: PropTypes.object.isRequired,
    }
    render() {
      const { form } = this.context
      // what we do is basically rendering `ComponentToWrap`
      // with an added `form` prop, like a hook
      return (
        <ComponentToWrap {...this.props} form={form} />
      )
    }
  }
}
export default formed
