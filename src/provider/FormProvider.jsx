import React, { Children } from 'react'
import PropTypes from 'prop-types'

const initialFormState = {
  formName: 'Default',
  isForm1: false,
  isForm2: false,
  isPersonal: false,
  isPension: false,
  isIDPS: false
}

export const getForm = code => {
  var forms = {
    'form1': {
      ...initialFormState,
      formName: 'Form 1',
      isForm1: true,
      isPersonal: true
    },
    'form2': {
      ...initialFormState,
      formName: 'Form 2',
      isForm2: true,
      isPersonal: true
    },
    'idps': {
      ...initialFormState,
      formName: 'IDPS form',
      isIDPS: true
    }
  }
  return forms[code] || forms['form1']
}

class FormProvider extends React.Component {
  static propTypes = {
    form: PropTypes.object.isRequired,
  }
  static childContextTypes = {
    form: PropTypes.object.isRequired,
  }
  getChildContext() {
    const { form } = this.props
    return { form }
  }
  render() {
    return Children.only(this.props.children)
  }
}

export default FormProvider
