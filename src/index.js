import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import './index.css';
import App from './App';
import reducers from './reducers';
import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducers);

const Main = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

render(<Main />, document.getElementById('root'));
registerServiceWorker();
