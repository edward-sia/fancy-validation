import React from "react";
import FormHelperText from "@material-ui/core/FormHelperText";

class CheckboxGroup extends React.Component {

  handleChange = event => {
    const target = event.currentTarget;
    let valueArray = [...this.props.value] || [];

    if (target.checked) {
      valueArray.push(target.id);
    } else {
      valueArray.splice(valueArray.indexOf(target.id), 1);
    }

    this.props.onChange(this.props.id, valueArray);
  };

  handleBlur = () => {
    // take care of touched
    this.props.onBlur(this.props.id, true);
  };

  render() {
    const { value, error, touched, label, id, children } = this.props;

    return (
      <fieldset id={id}>
        <legend>{label}</legend>
        <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
          {React.Children.map(children, child => {
            return React.cloneElement(child, {
              field: {
                value: value.includes(child.props.id),
                onChange: this.handleChange,
                onBlur: this.handleBlur
              }
            });
          })}
        </div>
        { touched && !!error && <FormHelperText>{error}</FormHelperText> }
      </fieldset>
    );
  }
}

export default CheckboxGroup
