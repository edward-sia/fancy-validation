import React from 'react'
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const StyledSelect = props => {
  const { touched, error, name, label, options, value, setFieldValue, setFieldTouched } = props;
  return (
    <FormControl id={name} error={touched && !!error}>
      <InputLabel htmlFor={name}>{label}</InputLabel>
      <Select
        onChange={(e) => setFieldValue(name, e.target.value)}
        onBlur={() => setFieldTouched(name)}
        value={value} 
        inputProps={{ name: name, id: name }}
      >
        {
          options.map((option, i) => <MenuItem value={option.value} key={i}>{option.label}</MenuItem>)
        }
      </Select>
      { touched && error && <FormHelperText>{error}</FormHelperText> }
    </FormControl>
  )
}

export default StyledSelect;
