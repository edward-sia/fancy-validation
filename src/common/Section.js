import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(16),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class Section extends React.Component {
  state = {
    expanded: false
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.expanded !== this.props.expanded) {
      this.setState({ expanded: nextProps.expanded })
    }
  }

  handleChange = () => {
    this.setState({
      expanded: !this.state.expanded,
    });
  };

  render() {
    const { summary, children, classes } = this.props
    const { expanded } = this.state

    return (
      <ExpansionPanel className={classes.root} expanded={expanded} onChange={(e, expanded) => this.handleChange()}>
        <ExpansionPanelSummary className={classes.heading} expandIcon={<ExpandMoreIcon />}>{ summary }</ExpansionPanelSummary>
        <ExpansionPanelDetails>{ children }</ExpansionPanelDetails>
      </ExpansionPanel>
    )
  }
}

export default withStyles(styles)(Section)