import React from 'react'
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const StyledTextField = props => {
  const {
    field,
    form,
    meta,
    label
  } = props;

  return (
    <FormControl error>
      <TextField
        {...field} 
        error={meta.touched && !!meta.error}
        label={label}
        // onBlur={form.handleBlur}
      />
      { (meta.touched && meta.error) && <FormHelperText>{meta.error}</FormHelperText> }
    </FormControl>
    )
}

export default StyledTextField
