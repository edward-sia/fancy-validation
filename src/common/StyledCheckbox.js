import React from "react";
import MuiCheckbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormGroup from '@material-ui/core/FormGroup'

const StyledCheckbox = ({
  field: { name, value, onChange, onBlur },
  form: { errors, touched },
  id,
  label,
  ...props
}) => {
  return (
    <FormGroup>
      <FormControlLabel control={
          <MuiCheckbox name={name} id={id} checked={value} onChange={onChange} onBlur={onBlur} {...props} />
        }
        label={label}
      />
      { touched[name] && <FormHelperText>{errors[name]}</FormHelperText> }
    </FormGroup>
  )
}

export default StyledCheckbox
