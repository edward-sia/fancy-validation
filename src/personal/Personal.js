import React from 'react'
import { FastField } from 'formik'
import { isEmpty } from '../utils';
import Section from '../common/Section';
import StyledTextField from '../common/StyledTextField';
import StyledSelect from '../common/StyledSelect';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '80%'
  }
}

const Personal = props => {
  const {
    values,
    touched = {},
    errors = {},
    expanded,
    setFieldValue,
    setFieldTouched
  } = props

  const genderOptions = [
    { value: '', label: '' },
    { value: 'male', label: 'Male' },
    { value: 'female', label: 'Female' }
  ]

  return (
    <Section summary='Personal' expanded={expanded || (!isEmpty(errors) && !isEmpty(touched))}>
      <div style={styles.container}>
        <FastField
          name='personal.firstName' label='First name *'
          component={StyledTextField}
        />
        <FastField
          name='personal.lastName' label='Last name *'
          component={StyledTextField}
        />
        <FastField
          name='personal.dob' label='Date of birth *'
          component={StyledTextField}
        />
        <StyledSelect
          name='personal.gender' label='Gender *'
          value={values.gender}
          setFieldTouched={setFieldTouched}
          setFieldValue={setFieldValue}
          error={errors.gender}
          touched={touched.gender}
          options={genderOptions}
        />
      </div>
    </Section>
  )
}

export default Personal
