import React from 'react';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import Button from '@material-ui/core/Button';
import Personal from '../personal/Personal'
import Contact from '../contact/Contact'
import Favourite from '../favourite/Favourite'
import FormLevelError from './FormLevelError';
import formed from '../provider/formed';

// Parent controls props to be distributed to individual sections via values
const mapPropsToValues = props => ({
  personal: props.initialValues.personal,
  contact: props.initialValues.contact,
  favourite: props.initialValues.favourite
})

// Parent controls validation schema
const validationSchema = Yup.object().shape({
  personal: Yup.object().shape({
    firstName: Yup.string().required('First name is required'),
    lastName: Yup.string().required('Last name is required'),
    dob: Yup.string().required('Date of birth is required'),
    gender: Yup.string().required('Gender is required')
  }),
  contact: Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Email is required'),
    mobile: Yup.number().typeError('Must be numeric').required('Mobile is required'),
    home: Yup.number().transform((cv, ov) => ov === '' ? undefined : cv).typeError('Must be numeric')
  }),
  favourite: Yup.object().shape({
    hobbies: Yup.array().required("At least one checkbox is required"),
  })
})

const handleSubmit = (values, { setSubmitting }) => {
  console.log('Submit pressed. - ', values)
  setSubmitting(false);
}

const ParentForm = props => {
  const {
    form: { formName },
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    handleSubmit
  } = props;
  // console.log('values ', values)
  // console.log('errors ', errors)
  // console.log('touched ', touched)
  
  const expanded = { personal: false, contact: false, favourite: false }

  // Parent form dictate what data to pass in and how to validate individual section
  return (
    <div>
      <p>{formName}</p>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <Personal values={values.personal} expanded={expanded.personal} errors={errors.personal} touched={touched.personal}
            handleChange={handleChange} handleBlur={handleBlur} setFieldValue={setFieldValue}
            setFieldTouched={setFieldTouched}></Personal>
          <Contact section='contact' values={values.contact} expanded={expanded.contact} errors={errors.contact} touched={touched.contact}
            handleChange={handleChange} handleBlur={handleBlur}></Contact>
          <Favourite values={values.favourite} expanded={expanded.favourite} errors={errors.favourite} touched={touched.favourite}
            setFieldValue={setFieldValue} setFieldTouched={setFieldTouched} />
          <Button type='submit' variant="contained" color="primary" style={{marginTop: '16px'}}>
            Submit form
          </Button>
          <FormLevelError object='personal' title='Error in Personal Section' />
          <FormLevelError object='contact' title='Error in Contact Section' />
          <FormLevelError object='favourite' title='Error in Favourite Section' />
        </div>
      </form>
    </div>
  )
}

const formedParentForm = formed(ParentForm)

export default withFormik({
  mapPropsToValues,
  validationSchema,
  handleSubmit
})(formedParentForm);