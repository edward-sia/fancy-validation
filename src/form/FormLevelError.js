import React from 'react';
import { connect, getIn } from 'formik';
import Button from '@material-ui/core/Button';
import { Link } from 'react-scroll';

const ErrorMessage = ({ goTo, message }) => (
  <Link activeClass="active" to={goTo} spy={true} smooth={true} duration={500}>
    <Button color="secondary">{message}</Button>
  </Link>
)

// This component renders an error message if a field has
// an error and it's already been touched.
const FormLevelError = props => {
  // All FormikProps available on props.formik!
  const errors = getIn(props.formik.errors, props.object);
  const touch = getIn(props.formik.touched, props.object);

  const errorArray = []
  for (var key in errors) {
    if (errors.hasOwnProperty(key)) {
      if (touch && touch.hasOwnProperty(key) && touch[key]) {
        errorArray.push({ message: errors[key], goTo: `${props.object}.${key}`})
      }
    }
  }
  const components = errorArray.map((error) => (
    <ErrorMessage goTo={error.goTo} key={error.goTo} message={error.message} />
  ))

  return errorArray.length > 0 ? <div><h3>{props.title}</h3>{components}</div> : null;
};

export default connect(FormLevelError);
