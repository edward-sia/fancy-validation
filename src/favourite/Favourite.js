import React from 'react'
import FormControl from '@material-ui/core/FormControl';
import { FastField } from "formik";
import Section from '../common/Section';
import CheckboxGroup from '../common/CheckboxGroup';
import StyledCheckbox from '../common/StyledCheckbox';
import { isEmpty } from '../utils';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '80%'
  }
}

const hobbyList = [
  { id: 'snowboarding', label: 'Snowboarding' },
  { id: 'skiing', label: 'Skiing' },
  { id: 'reading', label: 'Reading' },
  { id: 'swimming', label: 'Swimming' },
  { id: 'hiking', label: 'Hiking' },
  { id: 'golf', label: 'Golf' },
  { id: 'travel', label: 'Travel' },
  { id: 'rockclimbing', label: 'Rock climbing' },
  { id: 'eating', label: 'Eating' },
  { id: 'sleeping', label: 'Sleeping' }
]

const Favourite = props => {
  const { values, touched = {}, errors = {}, expanded, setFieldValue, setFieldTouched } = props

  return (
    <Section summary='Favourite' expanded={expanded || (!isEmpty(errors) && !isEmpty(touched))}>
      <div style={styles.container}>
        <FormControl required error={touched.hobbies && !!errors.hobbies}>
          <CheckboxGroup
            id='favourite.hobbies'
            label='Hobbies (select at least 1)'
            value={values.hobbies}
            error={errors.hobbies}
            touched={touched.hobbies}
            onChange={setFieldValue}
            onBlur={setFieldTouched}
          >
            {
              hobbyList.map(hobby => (
                <FastField
                  component={StyledCheckbox}
                  key={hobby.id}
                  name='favourite.hobbies'
                  id={hobby.id}
                  label={hobby.label}
                />
              ))
            }
          </CheckboxGroup>
        </FormControl>
      </div>
    </Section>
  )
}

export default Favourite
