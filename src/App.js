import React from 'react';
import logo from './logo.svg';
import './App.css';
import ParentForm from './form/ParentForm';
import FormProvider, { getForm } from './provider/FormProvider';

const containerStyle = {
  padding: '20px 40px'
}

const initialValues = {
  personal: {
    firstName: '',
    lastName: '',
    dob: '',
    gender: ''
  },
  contact: {
    email: '',
    mobile: '',
    home: ''
  },
  favourite: {
    hobbies: []
  }
}

const App = () => (
  <div>
    <header className="App App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h1 className="App-title">Welcome to Fancy Validation</h1>
    </header>
    <div style={containerStyle}>
      <FormProvider form={getForm('form1')}><ParentForm initialValues={initialValues} /></FormProvider>
    </div>
  </div>
)

export default App;
