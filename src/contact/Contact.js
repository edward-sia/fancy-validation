import React from 'react'
import { FastField } from 'formik';
import Section from '../common/Section';
import { isEmpty } from '../utils';
import StyledTextField from '../common/StyledTextField';
import formed from '../provider/formed';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '80%'
  }
}

const Contact = props => {
  const { errors = {}, touched = {}, expanded, section, form: { isIDPS } } = props

  return (
    <Section summary='Contact' expanded={expanded || (!isEmpty(errors) && !isEmpty(touched))}>
      <div style={styles.container}>
        <FastField
          name={`${section}.email`} label='Email *'
          component={StyledTextField}
        />
        <FastField
          name={`${section}.mobile`} label='Mobile phone *'
          component={StyledTextField}
        />
        {
          !isIDPS && <FastField name={`${section}.home`} label='Home phone'
            component={StyledTextField}
          />
        }
      </div>
    </Section>
  )
}

const formedContact = formed(Contact)

export default formedContact
