- Material-ui
-- Presentation component

- Formik
-- Forms validation library for components

- Yup
-- Validation schema that complement with Formik library

- React-scroll
-- Fancy scrolling to the error state